FROM phusion/baseimage:latest
MAINTAINER phreekbird webmaster@phreekbird.net
# now that we pulled the latest version of "I cannot Linux"-Ubuntu
# lets update repolists, upgrade, dist-upgrade, and clean up all the things.
# this will be my base container to build everything else from.
#
# Set DEBIAN_FRONTEND to noninteractive mode to silence debconf issues.
# and make sure to install apt-utils before we do anything to shut the thing up!
# aint nobody got time for dat!
RUN DEBIAN_FRONTEND=noninteractive apt-get update -y && DEBIAN_FRONTEND=noninteractive apt-get install apt-utils -y && DEBIAN_FRONTEND=noninteractive apt-get upgrade -y && DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -y && apt-get clean -y
# install curl and logrotate
RUN DEBIAN_FRONTEND=noninteractive apt-get install logrotate -y
# remove logrotate for dpkg as we will do our own in the default_logrotate.
RUN mv /etc/logrotate.d/dpkg /etc/logrotate.d/dpkg.ORIG.BAK
# add the logrotate.conf file.
ADD https://gitlab.com/phreekbird_public/core/raw/master/logrotate.conf /etc/
ADD https://gitlab.com/phreekbird_public/core/raw/master/default_logrotate /etc/logrotate.d/
# Start the cron service, if its not already running.
RUN /etc/init.d/cron start
